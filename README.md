# Contacts:
Nikita Korzhavin

nik.korg94@gmail.com

89523597603

# Available tasks
Hi, here are the testing round tasks for Frontend School at T-Systems.

## 1 Broken page
This nice mosaic is totally broken, please fix it.

## 2 Maze
Help the orange square get out of the frightening maze.

Unfortunately, the orange square doesn't know how to move up and left.
You have to teach it how to do that.

## 3 Page from JPEG
We've lost sources of our main page, only one last screenshot was left.

Please help us compose this web-page again.

## 4 Algorithm in JS
Fibonacci has called. Seems he can’t recall his numbers, except for the first two: 1, 1.

Please create .js file, which Fibonacci could copypaste in his browser console and get any Nth number by inputing the N into it.

## How to pass
Fork from this repository, complete the tasks and add your contact info for further communication.

One more thing: be brave and creative :)
We are waiting for you to join our team!
