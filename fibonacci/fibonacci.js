console.log(
  'result:',
  (function Fibb(finalNum) {
    let fibPrev = 0;
    let fib = 1;

    if (finalNum === 0) {
      return 0;
    } else if (finalNum === 1) {
      return 1;
    }

    for (let i = 2; i <= finalNum; i++) {
      let newFib = fib + fibPrev;
      fibPrev = fib;
      fib = newFib;
    }

    return fib;
  })(), //  put needed num into these brackets
);
